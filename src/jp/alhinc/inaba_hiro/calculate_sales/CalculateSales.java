package jp.alhinc.inaba_hiro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {

		//エラー処理（コマンドライン引数が渡されているか確認）
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();

		Map<String, String> commodityNames = new HashMap<>();
		Map<String, Long> commoditySales = new HashMap<>();
		BufferedReader br = null;


		//支店定義ファイル読み込みメソッドの実行
		if(!fileRead(args[0], "branch.lst","支店", "^\\d{3}$", branchNames, branchSales)) {
			return;
		}

		//商品定義ファイル読み込みメソッドの実行
		if(!fileRead(args[0], "commodity.lst", "商品", "[A-Za-z0-9]{8}", commodityNames, commoditySales)) {
			return;
		}


		//売上ファイルの読み込み
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		//売上ファイルの抽出
		//エラー処理（ファイルであることの確認）
		for(int i = 0; i < files.length ; i++) {
			if (files[i].isFile() && files[i].getName().matches("[0-9]{8}.rcd$")){
				rcdFiles.add(files[i]);
			}
		}

		//売上ファイルのソート
		Collections.sort(rcdFiles);

		//エラー処理（売上ファイル連番確認）
		for(int i = 0; i < rcdFiles.size()-1; i++){
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i+1).getName().substring(0,8));
			if((latter - former) != 1) {
				System.out.println("売上ファイルが連番になっていません");
				return;
			}
		}

		//売上ファイルの集計
		for(int i =0; i < rcdFiles.size(); i++) {
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				List<String> rcdData = new ArrayList<>();

				//売上ファイルの中身をリストに格納
				String line;
				while((line = br.readLine()) != null) {
					rcdData.add(line);
				}

				//エラー処理（売上ファイルのフォーマット確認）
				if(rcdData.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}

				//エラー処理（売上ファイルの支店コードが支店定義ファイルのコードにあるか確認）
				if(!branchSales.containsKey(rcdData.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}

				//エラー処理（売上ファイルの商品コードが商品定義ファイルのコードにあるか確認）
				if(!commoditySales.containsKey(rcdData.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + "の商品コードが不正です");
					return;
				}

				//エラー処理（売上金額が数字なのか確認）
				if(!rcdData.get(2).matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//型変換
				long fileSale = Long.parseLong(rcdData.get(2));

				//売上の加算
				long saleAmount = branchSales.get(rcdData.get(0)) + fileSale;
				long commoditySaleAmount = commoditySales.get(rcdData.get(1)) + fileSale;

				//エラー処理（売上金額の桁数確認）
				if(saleAmount >= 10000000000L || commoditySaleAmount >= 10000000000L){
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				//売上を格納
				branchSales.put(rcdData.get(0), saleAmount);
				commoditySales.put(rcdData.get(1), commoditySaleAmount);
			}catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally {
				if(br != null) {
					try {
						br.close();
					}catch(IOException e){
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}



		//支店別売上ファイル出力メソッドの実行
		if(!fileWrite(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}

		//商品別売上ファイル出力メソッドの実行
		if(!fileWrite(args[0], "commodity.out", commodityNames, commoditySales)) {
			return;
		}
	}


	//ファイル読み込みメソッド
	public static boolean fileRead(String filePath, String listName, String fileName, String regex, Map<String, String> Names, Map<String, Long> Sales) {

		BufferedReader br = null;

		try {
			File file = new File(filePath, listName);

			//エラー処理（ファイルの存在の確認）
			if(!file.exists()) {
				System.out.println(fileName + "定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {
				String items[];
				items = line.split(",");

				//エラー処理（ファイルのフォーマット確認）
				if((items.length != 2) || (!items[0].matches(regex))){
					System.out.println(fileName + "定義ファイルのフォーマットが不正です");
					return false;
				}
				Names.put(items[0], items[1]);
				Sales.put(items[0], 0L);
			}

		}catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}


	//ファイル出力メソッド
	public static boolean fileWrite(String filePath, String outFileName, Map<String, String> Names, Map<String, Long> Sales) {
		BufferedWriter bw = null;
		try {
			File file = new File(filePath, outFileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(String key : Names.keySet()) {
				bw.write(key + "," + Names.get(key) + "," + Sales.get(key));
				bw.newLine();
			}

		}catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}




